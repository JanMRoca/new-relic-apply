# New Relic Apply #

Backend code test for New Relic by Jan Manrique Roca.

## How to run ##

**Installation**

In order to run this project it's necessary to install node and the project dependencies. 

- To install the dependencies required to execute the project run: `npm install --production`.
- To install the dependencies required to execute the project and it's test suite run: `npm install`.

**Execution**

Once desired dependencies have been installed:

- To run the project, use `npm start`.
- To run the test suite, use `npm test`.


## How to try ##

**Request format**

- The http endpoint to fetch contributors is: `http://localhost:4000/contributors?city=<City>&limit=<limit>`.
- It is necessary to add an Authorization header indicating the api key. 
- Api keys are stored in `./data/apiKeys.txt` in reverse order. The only api key provided is `abc123`, but you can add more modifying the referenced file (it works even in runtime!),

e.g: `http://localhost:4000/contributors?city=Barcelona&limit=50`.

Feel free to use the getContributors.sh script, with the following usage: `./getContributors.sh <apiKey> <city> <limit>`. Curl is required in order to use it.
