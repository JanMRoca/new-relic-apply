#!/bin/bash

if [ "$#" != 3 ]; then
    echo -e "Incorrect arguments: Usage is: \n$0 <apiKey> <city> <limit>"
    exit 1;
fi

curl -k --user :"$1" https://localhost:4000/contributors\?city=$2\&limit=$3
