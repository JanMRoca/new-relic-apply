"use strict";

const contributorService = require("../services/contributorService.js");

module.exports = {

    getContributors: async function (req, res) {
        const limit = req.query.limit;
        const city = req.query.city;

        if (!city) return res.status(400).send("City not specified.");
        if (!limit) return res.status(400).send("Limit not specified.");
        if (!["50", "100", "150"].includes(limit)) return res.status(400).send("Limit is not a correct value.");

        contributorService.getContributors(city, limit)
            .then(contribs => res.status(200).send(contribs))
            .catch(err => {
                console.log(`Error in contributor controller: ${err}`)
                return res.status(500).send(err);
            });
    }

};