"use strict";

const apiKeyService = require("../services/apiKeyService.js");

module.exports = {

    isLogged: async function (req, res, next) {
        let authKey;

        if (req.headers.authorization) {
            authKey=new Buffer(req.headers.authorization.split(" ")[1], 'base64').toString().split(":", 2)[1];
        }

        if (!authKey) return res.status(401).send("No API key was provided.");


        apiKeyService.existsKey(authKey)
            .then(() => next())
            .catch(() => res.status(401).send("Invalid API key."));
    }

};