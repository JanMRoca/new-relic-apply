"use strict";

module.exports = {

    decrypt: function (key) {
       return key.split("").reverse().join("");
    }

};