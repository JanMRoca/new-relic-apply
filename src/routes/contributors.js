"use strict";

const controller = require("./../controllers/contributors.js");

module.exports = function (router) {

    router.get("/contributors",
        controller.getContributors);
};