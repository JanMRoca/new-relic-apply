"use strict";

const auth = require("../handlers/authHandler.js");

module.exports = function (express) {
    const router = express.Router();

    //Protects entire service
    router.get("*", auth.isLogged);

    // Contributors routes
    require("./contributors.js")(router);

    return router;
};