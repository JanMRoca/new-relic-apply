"use strict";

const ip = "localhost";
const port = 4000;

const spdy = require("spdy");
const express = require("express");
const routes = require("./routes/index.js")(express);
const fs = require("fs");

const app = express();

app.use(routes);

const options = {
    key: fs.readFileSync(__dirname + '/../ssl/server.key'),
    cert:  fs.readFileSync(__dirname + '/../ssl/server.crt')
}

spdy
    .createServer(options, app)
    .listen(port, (error) => {
        if (error) {
            console.error(error);
            return process.exit(1);
        } else {
            console.log(`Server started at ${ip}:${port}`);
        }
})

module.exports = app;