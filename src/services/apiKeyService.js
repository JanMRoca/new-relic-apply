"use strict";

const apiFilePath = "./data/apiKeys.txt";
const decryptor = require("../helpers/reverseDecryptor.js");

function existsKey(key) {
    return new Promise(function (resolve, reject) {
        /*Read from file and decrypt, then compare*/

        const lineReader = require("readline").createInterface({
            input: require("fs").createReadStream(apiFilePath)
        });

        lineReader.on("line", (line) => {
            if (decryptor.decrypt(line) === key) resolve();
        });

        lineReader.on("close", () => {
            reject();
        });

    });
}

module.exports = {
    existsKey
}
