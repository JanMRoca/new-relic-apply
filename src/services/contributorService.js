"use strict";

const axios = require("axios");

const apiPath = "https://api.github.com";
const versionHeader = "application/vnd.github.v3+json";
const pagingLimit = 100;

async function getContributors(location, limit) {
    const url = `${apiPath}/search/users?q=location:${location}&sort=repositories&order=desc`;
    const promises = [];

    for (let i=0; i*pagingLimit<limit; ++i) {
        promises.push(axios.get(`${url}&page=${i+1}&per_page=${pagingLimit}`,
            { headers: { Accept: versionHeader }  }
        ));
    }

    let contribs = await Promise.all(promises);

    return contribs.slice(0, limit).map(contrib => contrib.data.items)
        .reduce((acc, contrib) =>  acc.concat(...contrib));
}

module.exports = {
    getContributors
}
