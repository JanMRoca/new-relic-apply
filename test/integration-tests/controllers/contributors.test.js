"use strict";

const app = require("../../../src/server.js");
const request = require("supertest");


describe("GET /contributors", function() {

    it("should FAIL if no city is specified", (done) => {
        request(app)
            .get("/contributors?limit=50")
            .auth("", apiKey)
            .end(function(err, res) {
                expect(res.statusCode).to.equal(400);
                expect(res.text).to.be.equal("City not specified.");
                done();
            });
    });

    it("should FAIL if no limit is specified", (done) => {
        request(app)
            .get("/contributors?city=Barcelona")
            .auth("", apiKey)
            .end(function(err, res) {
                expect(res.statusCode).to.equal(400);
                expect(res.text).to.be.equal("Limit not specified.");
                done();
            });
    });

    it("should FAIL if limit is positive and incorrect", (done) => {
        request(app)
            .get("/contributors?city=Barcelona&limit=30")
            .auth("", apiKey)
            .end(function(err, res) {
                expect(res.statusCode).to.equal(400);
                expect(res.text).to.be.equal("Limit is not a correct value.");
                done();
            });
    });

    it("should FAIL if limit is zero", (done) => {
        request(app)
            .get("/contributors?city=Barcelona&limit=0")
            .auth("", apiKey)
            .end(function(err, res) {
                expect(res.statusCode).to.equal(400);
                expect(res.text).to.be.equal("Limit is not a correct value.");
                done();
            });
    });

    it("should FAIL if limit is negative", (done) => {
        request(app)
            .get("/contributors?city=Barcelona&limit=-10")
            .auth("", apiKey)
            .end(function(err, res) {
                expect(res.statusCode).to.equal(400);
                expect(res.text).to.be.equal("Limit is not a correct value.");
                done();
            });
    });

    it("should GET specified amount of contributors", (done) => {
        request(app)
            .get("/contributors?city=Barcelona&limit=50")
            .auth("", apiKey)
            .end(function(err, res) {
                expect(res.statusCode).to.equal(200);
                expect(res.body).to.be.an("array");
                expect(res.body).to.not.be.empty;
                done();
            });
    }).timeout(5000);

});