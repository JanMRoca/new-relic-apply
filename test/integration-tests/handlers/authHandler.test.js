"use strict";

const app = require("../../../src/server.js");
const request = require("supertest");


describe("authentication", function() {

    it("should FAIL if no apikey is given", (done) => {
         request(app)
            .get("/contributors?limit=50")
            .end(function(err, res) {
                expect(res.statusCode).to.equal(401);
                expect(res.text).to.be.equal("No API key was provided.");
                done();
            });
    })

    it("should FAIL if apikey is invalid", (done) => {
         request(app)
            .get("/contributors?limit=50")
            .auth("", `fake-${apiKey}`)
            .end(function(err, res) {
                expect(res.statusCode).to.equal(401);
                expect(res.text).to.be.equal("Invalid API key.");
                done();
            });
    })

    it("should GET results if valid apikey is given", (done) => {
        request(app)
            .get("/contributors?city=salou&limit=50")
            .auth("", apiKey)
            .end(function(err, res) {
                expect(res.statusCode).to.equal(200);
                done();
            });
    }).timeout(5000);

});