"use strict";

const decryptor = require("../../../src/helpers/reverseDecryptor.js");

describe("reverseDecryptor", () => {

    describe("decrypt", () => {

        it("should reverse single words", () => {
            const decrypted = decryptor.decrypt("Hola");

            expect(decrypted).to.equal("aloH");
        });

       it("should reverse strings with spaces", () => {
            const decrypted = decryptor.decrypt("Helloا World!");

            expect(decrypted).to.equal("!dlroW اolleH");
        });

        it("should reverse UTF-8 characters correctly", () => {
            const decrypted = decryptor.decrypt("ひら");

            expect(decrypted).to.equal("らひ");
        });

        it("should reverse words without breaking newlines", () => {
            const decrypted = decryptor.decrypt("abc\n123");

            expect(decrypted).to.equal("321\ncba");
        })

    })

})