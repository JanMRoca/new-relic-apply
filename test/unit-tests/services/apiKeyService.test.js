"use strict";

const apiKeyService = require("../../../src/services/apiKeyService.js");

describe("apiKeyService", () => {

    describe("existsKey", () => {

        it("should fail when no key is provided", () => {

            return apiKeyService.existsKey("").then(
                (contributors) => {
                    assert(false, "Promise resolved when it shouldn't.");
                }, (err) => {
                    assert(true);
                }
            )
        });

        it("should fail when key is not valid", () => {

            return apiKeyService.existsKey("abc1234").then(
                (contributors) => {
                    assert(false, "Promise resolved when it shouldn't.");
                }, (err) => {
                    assert(true);
                }
            )
        });

        it("should complete when key is valid", () => {

            return apiKeyService.existsKey("abc123").then(
                (contributors) => {
                    assert(true, "Promise resolved when it shouldn't.");
                }, (err) => {
                    assert(false, "Promise resolved when it shouldn't.");
                }
            )
        });

    });

});