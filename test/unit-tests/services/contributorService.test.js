"use strict";

const contributorService = require("../../../src/services/contributorService.js");
const axios = require("axios");

const contribJson = {
    "data": {
        "items": [
            {
                "name": "charles_cuts",
                "url": "https://github.com/DarkManolo"
            }
        ]
    }
}

let dropables;

beforeEach(function () {
    dropables = sinon.collection;
});

afterEach(function () {
    dropables.restore();
});

describe("contributorService", () => {

    describe("getContributors", () => {

        it("should fail when the source is not available", () => {
            dropables.stub(axios, "get").rejects();

            return contributorService.getContributors("Barcelona", 100).then(
                (contributors) => {
                    assert(false, "Promise resolved when it shouldn't.");
                }, (err) => {
                    assert(true);
                }
            )
        });

        it("should return an array of contributors", () => {
            dropables.stub(axios, "get").resolves(contribJson);

            return contributorService.getContributors("Barcelona", 1).then(
                (contributors) => {
                    expect(axios.get).to.have.been.calledOnce;
                    expect(contributors).to.be.an("array");
                    expect(contributors[0]).to.be.an("object").
                        that.has.all.keys("name", "url");
                }, (err) => {
                    assert(false, "Promise rejected when it shouldn't.");
                }
            )
        });

        it("should perform two http requests when limit is higher than specified by the source", () => {
            dropables.stub(axios, "get").resolves(contribJson);
    
            return contributorService.getContributors("Barcelona", 150).then(
                (contributors) => {
                    expect(axios.get).to.have.been.calledTwice;
                    expect(contributors).to.be.an("array");
                    expect(contributors).to.have.lengthOf(2);
                }, (err) => {
                    assert(false, "Promise rejected when it shouldn't.");
                }
            )
        });

    })

})